$(document).ready(function() {

   var ajaxurl  = Drupal.settings.basePath + 'similartitle/getlist';
   var nodetype = Drupal.settings.similartitle.nodetype;
   var nid      = Drupal.settings.similartitle.nid;

   function setSimilarTitle(res) {
      if(res == '') {
        $('div#similartitle').remove();
      }
      else {
         if($('div#similartitle').length == 0) {
           var stdiv = '<div id="similartitle">' + res + '</div>';
           $('#edit-title-wrapper').append(stdiv);
         }
         else {
           $('div#similartitle').html(res);
         }
      }
   }

  function refreshSimilarTitle() {
     strTitle = $('input#edit-title').val();
     if(strTitle.length < 3) {
       setSimilarTitle('');
     }
     else {
        $.get(ajaxurl, {
               type: nodetype , 
               string: strTitle,
               nid: nid 
             },
             function(data) {
                 setSimilarTitle(data);
             }
       );     
     }
  }

   $("#edit-title").blur(function() {
    setTimeout(function(){
        $('div#similartitle').remove();   
    }, 250);
   });

   $("#edit-title").focus(function() {
     refreshSimilarTitle();
   });

   $("#edit-title").keyup(function(e) {
     refreshSimilarTitle();
  });

});
